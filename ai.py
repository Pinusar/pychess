from copy import deepcopy

piece_values = {'P': 1, 'N': 3, 'B': 3, 'R': 5, 'Q': 9, 'K': 0}


def opponent_for(player):
    return 'BLACK' if player == 'WHITE' else 'WHITE'


def get_cell_value(board, cell):
    piece = board.board[cell.row][cell.column].upper()
    return piece_values[piece]


def count_material(board, player):
    cells_with_pieces = board.get_cells_with_pieces_by_player(player)
    return sum(map(lambda cell: get_cell_value(board, cell), cells_with_pieces))


def evaluate_board(board, player):
    return count_material(board, player) - count_material(board, opponent_for(player))


def get_move_options_for_player(game, player):
    options = {}
    possible_moves = game.board.get_possible_moves_for_player(player)
    for move in possible_moves:
        simulation = deepcopy(game)
        simulation.play_move(move)
        options[move] = count_material(simulation.board, player)
    return options
