import tkinter as tk
from tkinter import PhotoImage
from cell import Cell
from game import Game
from move import Move
from single_player_game import SinglePlayerGame

window = tk.Tk()


def new_single_player_game():
    room['current_game'] = SinglePlayerGame()
    draw_board()
    update_moves_history()


def new_multiplayer_game():
    room['current_game'] = Game()
    draw_board()
    update_moves_history()


buttons = tk.Frame()
buttons.pack()
btn_new_single_game = tk.Button(master=buttons, text='New single player game', command=new_single_player_game)
btn_new_single_game.pack(side=tk.LEFT)
btn_new_multiplayer_game = tk.Button(master=buttons, text='New multiplayer game', command=new_multiplayer_game)
btn_new_multiplayer_game.pack(side=tk.LEFT)

moves_history_container = tk.Frame()
moves_history_container.pack(side=tk.RIGHT)
moves_history_lbl = tk.Label(master=moves_history_container, text='Moves history:')
moves_history_lbl.pack(side=tk.TOP)
moves_info = tk.Text(master=moves_history_container, width=20, borderwidth=0)
moves_info.insert(1.0, 'Greetings')
moves_info.pack(side=tk.TOP)

chess_board = tk.Frame()
chess_board.pack()
game = SinglePlayerGame()
room = {'current_game': game}
next_move = []


def update_moves_history():
    moves_info.configure(state='normal')
    moves_info.delete(1.0, tk.END)
    moves = '\n'.join(list(map(lambda x: x.__repr__(), room['current_game'].moves_history)))
    moves_info.insert(1.0, moves)


def handle_click(row, column):
    next_move.append(Cell(row, column))
    if len(next_move) == 2:
        move = Move(next_move[0], next_move[1])
        try:
            room['current_game'].play_move(move)
            draw_board()
            if type(room['current_game']) is SinglePlayerGame:
                room['current_game'].play_computer_move()
                draw_board()
            update_moves_history()
        except RuntimeError as e:
            print(e)
        except ValueError as e:
            print(e)
        next_move.clear()


def draw_board():
    for row in range(8):
        for column in range(8):
            frame = tk.Frame(master=chess_board)
            frame.grid(row=8 - row, column=column)
            bg_color = 'yellow' if (row + column) % 2 == 1 else 'brown'
            piece = room['current_game'].board.board[row][column]
            if piece == '.':
                piece = 'x'
            img_path = f'img/{piece}.png'
            piece_img = PhotoImage(file=img_path)
            piece_img = piece_img.subsample(2)
            btn = tk.Button(master=frame,
                            image=piece_img,
                            height=100,
                            width=100,
                            bg=bg_color,
                            command=lambda x=row, y=column: handle_click(x, y))
            btn.image = piece_img
            btn.pack()
        row_label = tk.Label(master=chess_board, text=row + 1, width=4)
        row_label.grid(row=8 - row, column=9)

    for i in range(8):
        column_txt = Cell.get_columns()[i]
        column_label = tk.Label(master=chess_board, text=column_txt)
        column_label.grid(row=9, column=i)


draw_board()


if __name__ == '__main__':
    window.mainloop()
