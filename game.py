from board import Board
from cell import Cell
from move import Move

from copy import deepcopy


class Game:
    def __init__(self):
        self.board = Board()
        self.board.initialize()
        self.active_player = 'WHITE'
        self.moves_history = []
        self.pieces_that_have_moved = {
            Cell.from_string('e1'): False,
            Cell.from_string('a1'): False,
            Cell.from_string('h1'): False,
            Cell.from_string('e8'): False,
            Cell.from_string('a8'): False,
            Cell.from_string('h8'): False,
        }

    def play_move(self, move):
        if self.is_move_legal(move):
            print(f'Playing move from {move.cell_from} to {move.cell_to}')
            self.execute_move(move)
            self.notify_of_check()
            self.switch_active_player()
            self.moves_history.append(move)
        else:
            raise RuntimeError(f'Move {move} is illegal!')

    def play_move_from_string(self, move_string):
        cell_from, cell_to = move_string.split(' ')
        self.play_move(Move(Cell.from_string(cell_from), Cell.from_string(cell_to)))

    def execute_move(self, move):
        if self.is_castling_move(move):
            self.castle(move)
        else:
            self.move_piece(move.cell_from, move.cell_to)

    def move_piece(self, cell_from, cell_to):
        piece_to_move = self.board.board[cell_from.row][cell_from.column]
        self.board.board[cell_from.row][cell_from.column] = '.'
        self.board.board[cell_to.row][cell_to.column] = piece_to_move
        if cell_from in self.pieces_that_have_moved:
            self.pieces_that_have_moved[cell_from] = True

    def switch_active_player(self):
        self.active_player = 'BLACK' if self.active_player == 'WHITE' else 'WHITE'

    def is_move_legal(self, move):
        if self.is_castling_move(move):
            return self.is_castling_legal(move)
        return self.board.contains_own_piece(move.cell_from, self.active_player) \
               and self.cell_to_is_valid(move.cell_to) \
               and move.cell_to in self.board.get_possible_moves_for_piece(move.cell_from)\
               and not self.move_would_put_player_under_check(move, self.active_player)

    def cell_to_is_valid(self, cell_to):
        return self.board.is_empty(cell_to) or self.board.contains_opponent_piece(cell_to, self.active_player)

    def move_would_put_player_under_check(self, move, player):
        simulation = deepcopy(self)
        simulation.move_piece(move.cell_from, move.cell_to)
        return simulation.board.is_player_under_check(player)

    def notify_of_check(self):
        opponent = 'BLACK' if self.active_player == 'WHITE' else 'WHITE'
        if self.board.is_player_under_check(opponent):
            print(f'Player {opponent} is under check!')

    @staticmethod
    def is_castling_move(move):
        return move.cell_from == Cell(0, 4) and move.cell_to == Cell(0, 2) \
               or move.cell_from == Cell(0, 4) and move.cell_to == Cell(0, 6) \
               or move.cell_from == Cell(7, 4) and move.cell_to == Cell(7, 2) \
               or move.cell_from == Cell(7, 4) and move.cell_to == Cell(7, 6)

    def castle(self, move):
        king_from = move.cell_from
        king_to = move.cell_to
        rook_from = self.find_rook_starting_position(king_to)
        rook_to = self.find_rook_target_position(king_to)
        self.move_piece(king_from, king_to)
        self.move_piece(rook_from, rook_to)

    @staticmethod
    def find_rook_starting_position(king_to):
        rook_starting_positions_map = {
            'C1': 'A1',
            'G1': 'H1',
            'C8': 'A8',
            'G8': 'H8'
        }
        return Cell.from_string(rook_starting_positions_map[king_to.__str__()])

    @staticmethod
    def find_rook_target_position(king_to):
        rook_target_positions_map = {
            'C1': 'D1',
            'G1': 'F1',
            'C8': 'D8',
            'G8': 'F8'
        }
        return Cell.from_string(rook_target_positions_map[king_to.__str__()])

    def is_castling_legal(self, move):
        king_to = move.cell_to
        rook_from = self.find_rook_starting_position(king_to)
        if self.pieces_that_have_moved[move.cell_from]:
            print(f'Cannot castle. King on {move.cell_from} has already moved')
            return False
        if self.pieces_that_have_moved[rook_from]:
            print(f'Cannot castle. Rook on {rook_from} has already moved')
            return False
        if not self.board.horizontal_path_is_free(move.cell_from, rook_from):
            print(f'Cannot castle. Path from {move.cell_from} to {rook_from} is blocked.')
            return False
        if self.board.horizontal_path_has_cells_under_check(move.cell_from, move.cell_to, self.active_player):
            return False
        return True

    def get_legal_moves_for_player(self, player):
        legal_moves = []
        possible_moves = self.board.get_possible_moves_for_player(player)
        for move in possible_moves:
            if not self.move_would_put_player_under_check(move, self.active_player):
                legal_moves.append(move)
        return legal_moves

    def reset(self):
        self.board.initialize()
        self.active_player = 'WHITE'
        self.pieces_that_have_moved = {
            Cell.from_string('e1'): False,
            Cell.from_string('a1'): False,
            Cell.from_string('h1'): False,
            Cell.from_string('e8'): False,
            Cell.from_string('a8'): False,
            Cell.from_string('h8'): False,
        }
