from board import Board
from cell import Cell


def test_get_cells_with_pieces_by_player():
    board = Board()
    board.board[1][1] = 'Q'
    board.board[4][6] = 'B'

    assert board.get_cells_with_pieces_by_player('WHITE') == [Cell(1, 1), Cell(4, 6)]


def test_is_cell_under_attack_for_player():
    board = Board()
    board.board[1][1] = 'Q'

    assert board.is_cell_under_attack_for_player(Cell(7, 7), 'BLACK')
    assert not board.is_cell_under_attack_for_player(Cell(0, 3), 'BLACK')


def test_find_king_cell_for_player():
    board = Board()
    board.board[4][5] = 'k'

    assert board.find_king_cell_for_player('BLACK') == Cell(4, 5)


def test_is_player_under_check_when_under_check():
    board = Board()
    board.board[1][1] = 'Q'
    board.board[1][4] = 'k'

    assert board.is_player_under_check('BLACK') is True


def test_is_player_under_check_when_check_is_blocked():
    board = Board()
    board.board[1][1] = 'Q'
    board.board[1][3] = 'p'
    board.board[1][4] = 'k'

    assert board.is_player_under_check('BLACK') is False
