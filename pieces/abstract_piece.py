from cell import Cell


class AbstractPiece:
    def __init__(self, color):
        self.color = color

    @property
    def is_white(self):
        return self.color == 'WHITE'

    def get_possible_moves(self, cell_from, board):
        raise RuntimeError("Trying to call abstract method!")

    def append_if_cell_within_bounds(self, possible_moves, cell_from, board, row_offset, column_offset):
        starting_row = cell_from.row
        starting_column = cell_from.column
        cell_to_append = Cell(starting_row + row_offset, starting_column + column_offset)

        if cell_to_append.is_within_bounds() and (not board.contains_own_piece(cell_to_append, self.color)):
            contains_own_piece = (not board.contains_own_piece(cell_to_append, self.color))
            possible_moves.add(cell_to_append)

    def add_possible_moves_vertically(self, possible_moves, cell_from, board, vertical_offset=1):
        starting_row = cell_from.row
        starting_column = cell_from.column
        limit = 8 if vertical_offset == 1 else -1
        for i in range(starting_row + vertical_offset, limit, vertical_offset):
            cell = Cell(i, starting_column)
            if not board.contains_own_piece(cell, self.color):
                possible_moves.add(cell)
            if not board.is_empty(cell):
                break

    def add_possible_moves_horizontally(self, possible_moves, cell_from, board, horizontal_offset=1):
        starting_row = cell_from.row
        starting_column = cell_from.column
        limit = 8 if horizontal_offset == 1 else -1
        for i in range(starting_column + horizontal_offset, limit, horizontal_offset):
            cell = Cell(starting_row, i)
            if not board.contains_own_piece(cell, self.color):
                possible_moves.add(cell)
            if not board.is_empty(cell):
                break

    def add_possible_moves_diagonally(self, possible_moves, cell_from, board, row_offset, column_offset):
        starting_row = cell_from.row
        starting_column = cell_from.column
        current_column = starting_column
        limit = 8 if row_offset == 1 else -1
        for i in range(starting_row + row_offset, limit, row_offset):
            current_column += column_offset
            if i < 0 or i > 7 or current_column < 0 or current_column > 7:
                return
            cell = Cell(i, current_column)
            if not board.contains_own_piece(cell, self.color):
                possible_moves.add(cell)
            if not board.is_empty(cell):
                return
