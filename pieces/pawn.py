from pieces.abstract_piece import AbstractPiece
from cell import Cell


class Pawn(AbstractPiece):
    def __init__(self, color):
        super().__init__(color)

    def get_possible_moves(self, cell_from, board):
        possible_moves = set()
        starting_row = cell_from.row
        starting_column = cell_from.column
        one_cell_forward = Cell(starting_row + 1 if self.is_white else starting_row - 1, starting_column)
        if board.is_empty(one_cell_forward):
            possible_moves.add(one_cell_forward)
        if self.rank(starting_row) == 2:
            two_cell_forward = Cell(starting_row + 2 if self.is_white else starting_row - 2, starting_column)
            if board.vertical_path_is_free(cell_from, two_cell_forward) and board.is_empty(two_cell_forward):
                possible_moves.add(two_cell_forward)
        cells_under_attack = (Cell(starting_row + 1, starting_column - 1), Cell(starting_row + 1, starting_column + 1)) if self.is_white \
            else (Cell(starting_row - 1, starting_column - 1), Cell(starting_row - 1, starting_column + 1))
        for cell in cells_under_attack:
            if cell.is_within_bounds() and board.contains_opponent_piece(cell, self.color):
                possible_moves.add(cell)
        return possible_moves

    def rank(self, starting_row):
        if self.is_white:
            return starting_row + 1
        else:
            return 8 - starting_row
