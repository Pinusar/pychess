from pieces.abstract_piece import AbstractPiece


class King(AbstractPiece):
    def __init__(self, color):
        super().__init__(color)

    def get_possible_moves(self, cell_from, board):
        possible_moves = set()
        self.append_if_cell_within_bounds(possible_moves, cell_from, board, 1, -1)
        self.append_if_cell_within_bounds(possible_moves, cell_from, board, 1, 0)
        self.append_if_cell_within_bounds(possible_moves, cell_from, board, 1, 1)
        self.append_if_cell_within_bounds(possible_moves, cell_from, board, 0, 1)
        self.append_if_cell_within_bounds(possible_moves, cell_from, board, -1, 1)
        self.append_if_cell_within_bounds(possible_moves, cell_from, board, -1, 0)
        self.append_if_cell_within_bounds(possible_moves, cell_from, board, -1, -1)
        self.append_if_cell_within_bounds(possible_moves, cell_from, board, 0, -1)
        return possible_moves
