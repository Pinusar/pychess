from pieces.abstract_piece import AbstractPiece


class Rook(AbstractPiece):
    def __init__(self, color):
        super().__init__(color)

    def get_possible_moves(self, cell_from, board):
        possible_moves = set()
        self.add_possible_moves_vertically(possible_moves, cell_from, board, 1)
        self.add_possible_moves_vertically(possible_moves, cell_from, board, -1)
        self.add_possible_moves_horizontally(possible_moves, cell_from, board, 1)
        self.add_possible_moves_horizontally(possible_moves, cell_from, board, -1)
        return possible_moves
