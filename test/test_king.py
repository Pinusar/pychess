from cell import Cell
from game import Game
from pieces.king import King


def test_get_possible_moves():
    game = Game()
    game.play_move_from_string('e2 e4')
    game.play_move_from_string('e7 e5')
    game.play_move_from_string('e1 e2')
    game.play_move_from_string('d7 d5')
    king = King('WHITE')

    possible_moves = king.get_possible_moves(Cell.from_string('e2'), game.board)

    assert possible_moves == {Cell.from_string('d3'), Cell.from_string('e3'), Cell.from_string('f3'),
                              Cell.from_string('e1')}
