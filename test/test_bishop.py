from cell import Cell
from game import Game
from pieces.bishop import Bishop


def test_get_possible_moves():
    game = Game()
    game.play_move_from_string('e2 e4')
    game.play_move_from_string('e7 e5')
    game.play_move_from_string('f1 b5')
    game.play_move_from_string('c7 c6')
    bishop = Bishop('WHITE')

    possible_moves = bishop.get_possible_moves(Cell.from_string('b5'), game.board)

    assert possible_moves == {Cell.from_string('a6'), Cell.from_string('a4'), Cell.from_string('c6'),
                              Cell.from_string('c4'), Cell.from_string('d3'), Cell.from_string('e2'),
                              Cell.from_string('f1')}
