import ai
from board import Board
from game import Game


def test_count_material_black():
    board = Board()
    board.board[0][1] = 'r'
    board.board[1][1] = 'p'

    result = ai.count_material(board, 'BLACK')

    assert result == 6


def test_count_material_start_of_game():
    game = Game()

    result = ai.count_material(game.board, 'WHITE')

    assert result == 39


def test_evaluate_board():
    board = Board()
    board.board[7][1] = 'r'
    board.board[6][1] = 'p'
    board.board[0][0] = 'R'
    board.board[0][3] = 'Q'

    result = ai.evaluate_board(board, 'WHITE')

    assert result == 8


def test_get_move_options_for_player():
    game = Game()
    game.play_move_from_string('e2 e4')
    game.play_move_from_string('d7 d5')

    result = ai.get_move_options_for_player(game, 'WHITE')

    print(result)
