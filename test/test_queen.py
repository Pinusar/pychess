from cell import Cell
from game import Game
from pieces.queen import Queen


def test_get_possible_moves():
    game = Game()
    game.play_move_from_string('e2 e4')
    game.play_move_from_string('e7 e5')
    game.play_move_from_string('d1 h5')
    game.play_move_from_string('g7 g5')
    queen = Queen('WHITE')

    possible_moves = queen.get_possible_moves(Cell.from_string('h5'), game.board)

    assert possible_moves == {Cell.from_string('h6'),
                              Cell.from_string('h7'),
                              Cell.from_string('h4'),
                              Cell.from_string('h3'),
                              Cell.from_string('g6'),
                              Cell.from_string('g5'),
                              Cell.from_string('g4'),
                              Cell.from_string('f7'),
                              Cell.from_string('f3'),
                              Cell.from_string('e2'),
                              Cell.from_string('d1')}
