from cell import Cell
from game import Game
from move import Move


def test_is_empty():
    game = Game()
    assert not game.board.is_empty(Cell(1, 2))
    assert game.board.is_empty(Cell(3, 3))


def test_play_move_from_string():
    game = Game()

    game.play_move_from_string('e2 e4')
    game.play_move_from_string('e7 e5')
    game.play_move_from_string('g1 f3')
    game.play_move_from_string('b8 c6')
    game.play_move_from_string('f1 b5')

    assert game.board.board[3][4] == 'P'
    assert game.board.board[2][5] == 'N'
    assert game.board.board[4][1] == 'B'
    assert game.board.board[4][4] == 'p'
    assert game.board.board[5][2] == 'n'


def test_move_would_put_player_under_check():
    game = Game()
    game.play_move_from_string('e2 e4')
    game.play_move_from_string('e7 e5')
    game.play_move_from_string('d1 h5')

    assert game.move_would_put_player_under_check(Move(Cell.from_string('f7'), Cell.from_string('f6')),
                                                  game.active_player)
    assert not game.move_would_put_player_under_check(Move(Cell.from_string('g7'), Cell.from_string('g6')),
                                                      game.active_player)


def test_is_castling_move_castle_kingside():
    cell_from = Cell.from_string('e1')
    cell_to = Cell.from_string('g1')
    assert Game.is_castling_move(Move(cell_from, cell_to)) is True


def test_is_castling_move_castle_queenside_black():
    cell_from = Cell.from_string('e8')
    cell_to = Cell.from_string('c8')
    assert Game.is_castling_move(Move(cell_from, cell_to)) is True


def test_is_castling_move_illegal_move():
    cell_from = Cell.from_string('e1')
    cell_to = Cell.from_string('f4')
    assert Game.is_castling_move(Move(cell_from, cell_to)) is False


def test_castle_kingside():
    game = Game()
    game.play_move_from_string('e2 e4')
    game.play_move_from_string('e7 e5')
    game.play_move_from_string('g1 f3')
    game.play_move_from_string('b8 c6')
    game.play_move_from_string('f1 b5')
    game.play_move_from_string('a7 a6')
    game.play_move_from_string('e1 g1')

    assert game.board.board[0][6] == 'K'
    assert game.board.board[0][5] == 'R'


def test_cannot_castle_when_king_has_moved():
    game = Game()
    game.play_move_from_string('e2 e4')
    game.play_move_from_string('e7 e5')
    game.play_move_from_string('g1 f3')
    game.play_move_from_string('b8 c6')
    game.play_move_from_string('f1 b5')
    game.play_move_from_string('a7 a6')
    game.play_move_from_string('e1 e2')
    game.play_move_from_string('a6 a5')
    game.play_move_from_string('e2 e1')
    game.play_move_from_string('a5 a4')

    try:
        game.play_move_from_string('e1 g1')
        assert False
    except RuntimeError as e:
        assert True


def test_cannot_castle_when_rook_has_moved():
    game = Game()
    game.play_move_from_string('e2 e4')
    game.play_move_from_string('e7 e5')
    game.play_move_from_string('g1 f3')
    game.play_move_from_string('b8 c6')
    game.play_move_from_string('f1 b5')
    game.play_move_from_string('a7 a6')
    game.play_move_from_string('h1 g1')
    game.play_move_from_string('a6 a5')
    game.play_move_from_string('g1 h1')
    game.play_move_from_string('a5 a4')

    try:
        game.play_move_from_string('e1 g1')
        assert False
    except RuntimeError as e:
        assert True


def test_cannot_castle_when_path_is_blocked():
    game = Game()
    game.play_move_from_string('e2 e4')
    game.play_move_from_string('e7 e5')
    game.play_move_from_string('g1 f3')
    game.play_move_from_string('b8 c6')

    try:
        game.play_move_from_string('e1 g1')
        assert False
    except RuntimeError as e:
        assert True


def test_cannot_castle_through_check():
    game = Game()
    game.play_move_from_string('e2 e4')
    game.play_move_from_string('b7 b6')
    game.play_move_from_string('g1 f3')
    game.play_move_from_string('c8 a6')
    game.play_move_from_string('f1 c4')
    game.play_move_from_string('h7 h6')
    game.play_move_from_string('c4 d5')
    game.play_move_from_string('h6 h5')

    try:
        game.play_move_from_string('e1 g1')
        assert False
    except RuntimeError as e:
        assert True


def test_cannot_castle_into_check():
    game = Game()
    game.play_move_from_string('e2 e4')
    game.play_move_from_string('e7 e5')
    game.play_move_from_string('f2 f4')
    game.play_move_from_string('f8 c5')
    game.play_move_from_string('f1 c4')
    game.play_move_from_string('h7 h6')
    game.play_move_from_string('g1 h3')
    game.play_move_from_string('h6 h5')

    try:
        game.play_move_from_string('e1 g1')
        assert False
    except RuntimeError as e:
        assert True
