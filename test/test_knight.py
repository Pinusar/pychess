from cell import Cell
from game import Game
from pieces.knight import Knight


def test_get_possible_moves():
    game = Game()
    knight = Knight('WHITE')

    possible_moves = knight.get_possible_moves(Cell(0, 1), game.board)

    assert possible_moves == {Cell(2, 0), Cell(2, 2)}


def test_get_possible_moves_capture():
    game = Game()
    game.board.board[1][3] = 'b'
    knight = Knight('WHITE')

    possible_moves = knight.get_possible_moves(Cell(0, 1), game.board)

    assert possible_moves == {Cell(2, 0), Cell(2, 2), Cell(1, 3)}
