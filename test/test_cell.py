from cell import Cell


def test_from_string():
    a1 = Cell.from_string('a1')
    a4 = Cell.from_string('a4')
    d5 = Cell.from_string('d5')
    h8 = Cell.from_string('H8')

    assert a1 == Cell(0, 0)
    assert a4 == Cell(3, 0)
    assert d5 == Cell(4, 3)
    assert h8 == Cell(7, 7)
