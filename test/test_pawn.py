from cell import Cell
from game import Game
from move import Move
from pieces.pawn import Pawn


def test_get_possible_moves_one_step_forward():
    game = Game()
    game.play_move(Move(Cell(1, 1), Cell(2, 1)))
    pawn = Pawn('WHITE')

    possible_moves = pawn.get_possible_moves(Cell(2, 1), game.board)

    assert possible_moves == {Cell(3, 1)}


def test_get_possible_moves_two_steps_forward():
    game = Game()
    pawn = Pawn('WHITE')

    possible_moves = pawn.get_possible_moves(Cell(1, 1), game.board)

    assert possible_moves == {Cell(2, 1), Cell(3, 1)}


def test_get_possible_cant_move_forward_when_blocked():
    game = Game()
    game.board.board[2][1] = 'n'
    pawn = Pawn('WHITE')
    assert len(pawn.get_possible_moves(Cell(1, 1), game.board)) == 0


def test_get_possible_can_capture_diagonally():
    game = Game()
    game.board.board[2][1] = 'n'
    game.board.board[2][0] = 'b'
    pawn = Pawn('WHITE')
    assert pawn.get_possible_moves(Cell(1, 1), game.board) == {Cell(2, 0)}
