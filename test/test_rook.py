from cell import Cell
from game import Game
from pieces.rook import Rook


def test_get_possible_moves():
    game = Game()
    game.play_move_from_string('a2 a4')
    game.play_move_from_string('g7 g6')
    game.play_move_from_string('a1 a3')
    game.play_move_from_string('f8 g7')
    game.play_move_from_string('d2 d3')
    game.play_move_from_string('g7 c3')
    rook = Rook('WHITE')

    possible_moves = rook.get_possible_moves(Cell.from_string('a3'), game.board)

    assert possible_moves == {Cell.from_string('a2'), Cell.from_string('a1'), Cell.from_string('b3'),
                              Cell.from_string('c3')}
