from cell import Cell
from move import Move
from pieces.bishop import Bishop
from pieces.king import King
from pieces.knight import Knight
from pieces.pawn import Pawn
from pieces.queen import Queen
from pieces.rook import Rook


class Board:
    def __init__(self):
        self.board = [['.'] * 8 for i in range(8)]

    def __str__(self):
        result = ''
        for row in self.board:
            result += ''.join(row) + '\n'
        return result

    def initialize(self):
        self.board = [['.'] * 8 for i in range(8)]
        self.initialize_white()
        self.initialize_black()

    def initialize_white(self):
        for i in range(8):
            self.board[1][i] = 'P'
        self.board[0][0] = 'R'
        self.board[0][7] = 'R'
        self.board[0][1] = 'N'
        self.board[0][6] = 'N'
        self.board[0][2] = 'B'
        self.board[0][5] = 'B'
        self.board[0][3] = 'Q'
        self.board[0][4] = 'K'

    def initialize_black(self):
        for i in range(8):
            self.board[6][i] = 'p'
        self.board[7][0] = 'r'
        self.board[7][7] = 'r'
        self.board[7][1] = 'n'
        self.board[7][6] = 'n'
        self.board[7][2] = 'b'
        self.board[7][5] = 'b'
        self.board[7][3] = 'q'
        self.board[7][4] = 'k'

    def is_empty(self, cell):
        return self.board[cell.row][cell.column] == '.'

    def contains_own_piece(self, cell, active_player):
        piece = self.board[cell.row][cell.column]
        return not self.is_empty(cell) and piece.isupper() if active_player == 'WHITE' else piece.islower()

    def contains_opponent_piece(self, cell, active_player):
        piece = self.board[cell.row][cell.column]
        return not self.is_empty(cell) and piece.isupper() if active_player == 'BLACK' else piece.islower()

    def vertical_path_is_free(self, cell_from, cell_to):
        #TODO: needs testing
        if cell_from.column == cell_to.column:
            if cell_from.row < cell_to.row:
                for i in range(cell_from.row + 1, cell_to.row):
                    print(f"Checking if cell {Cell(i, cell_to.column)} is free.")
                    if self.board[i][cell_from.column] != '.':
                        return False
                return True
            else:
                for i in range(cell_from.row - 1, cell_to.row, -1):
                    print(f"Checking if cell {Cell(i, cell_to.column)} is free.")
                    if self.board[i][cell_from.column] != '.':
                        return False
                return True
        raise RuntimeError(f"Can't check if vertical path is free. Cells {cell_from} and {cell_to} are not on the same file.")

    def horizontal_path_is_free(self, cell_from, cell_to):
        if cell_from.row == cell_to.row:
            if cell_from.column < cell_to.column:
                for i in range(cell_from.column + 1, cell_to.column):
                    print(f"Checking if cell {Cell(cell_to.row, i)} is free.")
                    if self.board[cell_from.row][i] != '.':
                        return False
                return True
            else:
                for i in range(cell_from.column - 1, cell_to.column, -1):
                    print(f"Checking if cell {Cell(cell_to.row, i)} is free.")
                    if self.board[cell_from.row][i] != '.':
                        return False
                return True
        raise RuntimeError(f"Can't check if horizontal path is free. Cells {cell_from} and {cell_to} are not on the same row.")

    def horizontal_path_has_cells_under_check(self, cell_from, cell_to, active_player):
        if cell_from.row == cell_to.row:
            if cell_from.column < cell_to.column:
                for i in range(cell_from.column + 1, cell_to.column + 1):
                    print(f"Checking if cell {Cell(cell_to.row, i)} is under check.")
                    if self.is_cell_under_attack_for_player(Cell(cell_from.row, i), active_player):
                        return True
                return False
            else:
                for i in range(cell_from.column - 1, cell_to.column - 1, -1):
                    print(f"Checking if cell {Cell(cell_to.row, i)} is under check.")
                    if self.is_cell_under_attack_for_player(Cell(cell_from.row, i), active_player):
                        return True
                return False
        raise RuntimeError(f"Can't check if horizontal path has cells under check. Cells {cell_from} and {cell_to} are not on the same row.")

    def get_cells_with_pieces_by_player(self, active_player):
        cells = []
        for row in range(8):
            for column in range(8):
                cell = Cell(row, column)
                if not self.is_empty(cell) and self.contains_own_piece(cell, active_player):
                    cells.append(cell)
        return cells

    def get_possible_moves_for_piece(self, cell_from):
        starting_row = cell_from.row
        starting_column = cell_from.column
        piece = self.board[starting_row][starting_column]
        if piece.upper() == 'P':
            pawn = Pawn('WHITE' if piece.isupper() else 'BLACK')
            return pawn.get_possible_moves(cell_from, self)
        elif piece.upper() == 'N':
            knight = Knight('WHITE' if piece.isupper() else 'BLACK')
            return knight.get_possible_moves(cell_from, self)
        elif piece.upper() == 'B':
            bishop = Bishop('WHITE' if piece.isupper() else 'BLACK')
            return bishop.get_possible_moves(cell_from, self)
        elif piece.upper() == 'R':
            rook = Rook('WHITE' if piece.isupper() else 'BLACK')
            return rook.get_possible_moves(cell_from, self)
        elif piece.upper() == 'Q':
            queen = Queen('WHITE' if piece.isupper() else 'BLACK')
            return queen.get_possible_moves(cell_from, self)
        elif piece.upper() == 'K':
            king = King('WHITE' if piece.isupper() else 'BLACK')
            return king.get_possible_moves(cell_from, self)
        else:
            raise ValueError(f'Unknown piece to get moves for: {piece}')

    def is_cell_under_attack_for_player(self, cell_to, active_player):
        opponent = 'WHITE' if active_player == 'BLACK' else 'BLACK'
        cells_with_opponent_pieces = self.get_cells_with_pieces_by_player(opponent)
        for cell in cells_with_opponent_pieces:
            possible_moves = self.get_possible_moves_for_piece(cell)
            if cell_to in possible_moves:
                return True
        return False

    def is_player_under_check(self, player):
        king_cell = self.find_king_cell_for_player(player)
        return self.is_cell_under_attack_for_player(king_cell, player)

    def find_king_cell_for_player(self, player):
        target_piece = 'K' if player == 'WHITE' else 'k'
        for row in range(8):
            for column in range(8):
                if self.board[row][column] == target_piece:
                    return Cell(row, column)
        raise ValueError(f'Player {player} has no king on the board.')

    def get_possible_moves_for_player(self, player):
        possible_moves = []
        cells_with_pieces = self.get_cells_with_pieces_by_player(player)
        for piece_cell in cells_with_pieces:
            target_cells = self.get_possible_moves_for_piece(piece_cell)
            for target_cell in target_cells:
                possible_moves.append(Move(piece_cell, target_cell))
        return possible_moves
