class Cell:
    def __init__(self, row, column):
        self.row = row
        self.column = column

    def __str__(self):
        return f'{self.get_columns()[self.column]}{self.row + 1}'

    def __repr__(self):
        return f'{self.get_columns()[self.column]}{self.row + 1}'

    def __eq__(self, other):
        return self.row == other.row and self.column == other.column

    def __hash__(self):
        return self.row * self.column

    def is_within_bounds(self):
        return 8 > self.row >= 0 and 8 > self.column >= 0

    @staticmethod
    def get_columns():
        return ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']

    @staticmethod
    def from_string(cell_string):
        return Cell(int(cell_string[1]) - 1, Cell.get_columns().index(cell_string[0].upper()))
