from random import randint

from game import Game


class SinglePlayerGame(Game):
    def __init__(self):
        super().__init__()

    def play_computer_move(self):
        move = self.get_computer_move()
        if self.is_move_legal(move):
            print(f'Playing move from {move.cell_from} to {move.cell_to}')
            self.move_piece(move.cell_from, move.cell_to)
            self.notify_of_check()
            self.switch_active_player()
            self.moves_history.append(move)
        else:
            raise RuntimeError(f'Move {move} is illegal!')

    def get_computer_move(self):
        legal_moves = self.get_legal_moves_for_player(self.active_player)
        if len(legal_moves) == 0:
            raise ValueError('No legal moves left! Computer has lost!')
        return legal_moves[randint(0, len(legal_moves) - 1)]
