__Installation__

Install tkinter module:

`sudo apt install python3-tk`

Clone the repository:

`git clone https://gitlab.com/Pinusar/pychess.git`

Run the game:

`python3 gui.py`