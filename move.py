class Move:
    def __init__(self, cell_from, cell_to):
        self.cell_from = cell_from
        self.cell_to = cell_to

    def __str__(self):
        return f'A move from {self.cell_from} to {self.cell_to}'

    def __repr__(self):
        return f'{self.cell_from} {self.cell_to}'
